const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender","celadon","saffron","fuschia","cinnabar"];

let colorOptions = () => {
    var contentHTML = "";
    for (let i = 0; i < colorList.length; i++) {
        var eachButton = `<button onclick="colorSelect('${colorList[i]}')" class="color-button ${colorList[i]}"></button>`;
        contentHTML += eachButton;
    }
    document.getElementById("colorContainer").innerHTML = contentHTML;
};
colorOptions();

let colorSelect = (color) => {
    document.getElementById("house").classList.toggle(color);
}