let tinhDTB = (...diem) => {
    var diemTong = 0;
    for (let i of diem) {
        diemTong += i;
    }
    var DTB = diemTong / diem.length;
    return DTB;
}

let tinhDTBKhoi1 = () => {
    var diemToan = document.getElementById("inpToan").value * 1;
    var diemLy = document.getElementById("inpLy").value * 1;
    var diemHoa = document.getElementById("inpHoa").value * 1;
    var DTBKhoi1 = tinhDTB(diemToan, diemLy, diemHoa);
    document.getElementById("tbKhoi1").innerHTML = DTBKhoi1;
}

let tinhDTBKhoi2 = () => {
    var diemVan = document.getElementById("inpVan").value * 1;
    var diemSu = document.getElementById("inpSu").value * 1;
    var diemDia = document.getElementById("inpDia").value * 1;
    var diemEnglish = document.getElementById("inpEnglish").value * 1;
    var DTBKhoi2 = tinhDTB(diemVan, diemSu, diemDia, diemEnglish);
    document.getElementById("tbKhoi2").innerHTML = DTBKhoi2;
}