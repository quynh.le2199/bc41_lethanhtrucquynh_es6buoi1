let jumpText = () => {
    let stringContent = document.getElementById("heading").innerText;
    let stringChars = [...stringContent];
    var contentHTML = "";
    for (let i of stringChars) {
        var spanEl = `<span>${i}</span>`
        contentHTML += spanEl;
    }
    document.getElementById("heading").innerHTML = contentHTML;
}

jumpText();